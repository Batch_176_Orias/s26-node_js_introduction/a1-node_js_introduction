let http = require("http");

http.createServer(function(req, res){

	if(req.url === "/"){
		res.writeHead(200, {"Content-Type":"text-plain"});
		res.end("Welcome to B176 Booking System");
	}
	else if(req.url === "/courses"){
		res.writeHead(200, {"Content-Type":"text-plain"});
		res.end("Welcome to the Course Page. View our Courses");
	}
	else if(req.url === "/profile"){
		res.writeHead(200, {"Content-Type":"text-plain"});
		res.end("Welcome to your profile. View your details");
	}
	else{
		res.writeHead(404, {"Content-Type":"text-plain"});
		res.end("Resource not found");
	}
}).listen(5000);

console.log("We are now running at server 5000");